package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    void createTask(@Nullable String userId, @Nullable String taskName, @Nullable String projectName, @Nullable String description) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateStartDate(@Nullable String userId, @Nullable String id, @Nullable Date date) throws Exception;

    void updateCompleteDate(@Nullable String userId, @Nullable String id, @Nullable Date date) throws Exception;

    @NotNull
    Long countAllTasks();

    @NotNull
    Long countByUserId(@Nullable String userId) throws Exception;

    @NotNull
    Long countByProjectId(@Nullable String projectId) throws Exception;

    @NotNull
    Long countByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable String userId) throws Exception;

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String projectId) throws Exception;

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneByName(@Nullable String userId, @Nullable String name) throws Exception;

    void removeAll();

    void removeAllByUserId(@Nullable String userId) throws Exception;

    void removeAllByProjectId(@Nullable String projectId) throws Exception;

    void removeAllByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

}
