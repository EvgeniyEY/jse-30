package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.dto.SessionDTO;

import java.util.Date;
import java.util.List;

public interface ITaskEndpoint {

    void createTask(@Nullable SessionDTO sessionDTO, @Nullable String name, @Nullable String projectName, @Nullable String description) throws Exception;

    void updateTaskById(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateTaskStartDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date) throws Exception;

    void updateTaskCompleteDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date) throws Exception;

    @NotNull
    Long countAllTasks(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    Long countUserTasks(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    Long countProjectTasks(@Nullable SessionDTO sessionDTO, @Nullable String projectId) throws Exception;

    @NotNull
    Long countUserTasksOnProject(@Nullable SessionDTO sessionDTO, @Nullable String projectId) throws Exception;

    @NotNull
    TaskDTO findTaskById(@Nullable SessionDTO sessionDTO, @Nullable String id) throws Exception;

    @NotNull
    TaskDTO findTaskByName(@Nullable SessionDTO sessionDTO, @Nullable String name) throws Exception;

    @NotNull
    List<TaskDTO> findAllTasks(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    List<TaskDTO> findAllTasksByUserId(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    List<TaskDTO> findAllTasksByProjectId(@Nullable SessionDTO sessionDTO, @Nullable String projectId) throws Exception;

    @NotNull
    List<TaskDTO> findAllUserTasksOnProject(@Nullable SessionDTO sessionDTO, @Nullable String projectId) throws Exception;

    void removeTaskById(@Nullable SessionDTO sessionDTO, @Nullable String id) throws Exception;

    void removeTaskByName(@Nullable SessionDTO sessionDTO, @Nullable String name) throws Exception;

    void clearTasks(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearTasksByUserId(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearTasksByProjectId(@Nullable SessionDTO sessionDTO, @Nullable String projectId) throws Exception;

    void clearUserTasksOnProject(@Nullable SessionDTO sessionDTO, @Nullable String projectId) throws Exception;

}
