package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.dto.SessionDTO;

import java.util.Date;
import java.util.List;

public interface IProjectEndpoint {

    void createProject(@Nullable SessionDTO sessionDTO, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectById(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectStartDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date) throws Exception;

    void updateProjectCompleteDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date) throws Exception;

    @NotNull
    Long countAllProjects(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    Long countUserProjects(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    ProjectDTO findProjectById(@Nullable SessionDTO sessionDTO, @Nullable String id) throws Exception;

    @NotNull
    ProjectDTO findProjectByName(@Nullable SessionDTO sessionDTO, @Nullable String name) throws Exception;

    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    List<ProjectDTO> findAllProjectsByUserId(@Nullable SessionDTO sessionDTO) throws Exception;

    void removeProjectById(@Nullable SessionDTO sessionDTO, @Nullable String id) throws Exception;

    void removeProjectByName(@Nullable SessionDTO sessionDTO, @Nullable String name) throws Exception;

    void clearProjects(@Nullable SessionDTO sessionDTO) throws Exception;

    void clearProjectsByUserId(@Nullable SessionDTO sessionDTO) throws Exception;
    
}
