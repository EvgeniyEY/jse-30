package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.service.BackupService;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    BackupService getBackupService();

    @NotNull
    ISqlConnectionService getSqlConnectionService();

}
