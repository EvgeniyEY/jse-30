package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity {

    @Nullable
    @Column(columnDefinition = "TINYTEXT",
            nullable = false)
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date completeDate;

    @Nullable
    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

}
