package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.IAdminDataEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminDataEndpoint implements IAdminDataEndpoint {

    private ServiceLocator serviceLocator;

    public AdminDataEndpoint() {
    }

    public AdminDataEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void saveXmlByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().saveXmlByJaxb();
    }

    @Override
    @WebMethod
    public void loadXmlByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().loadXmlByJaxb();
    }

    @Override
    @WebMethod
    public void clearXmlFileJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().clearXmlFileJaxb();
    }

    @Override
    @WebMethod
    public void saveXmlByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().saveXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void loadXmlByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().loadXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void clearXmlFileFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().clearXmlFileFasterXml();
    }

    @Override
    @WebMethod
    public void saveJsonByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().saveJsonByJaxb();
    }

    @Override
    @WebMethod
    public void loadJsonByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().loadJsonByJaxb();
    }

    @Override
    @WebMethod
    public void clearJsonFileJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().clearJsonFileJaxb();
    }

    @Override
    @WebMethod
    public void saveJsonByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().saveJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void loadJsonByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().loadJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void clearJsonFileFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().clearJsonFileFasterXml();
    }

    @Override
    @WebMethod
    public void saveBinary(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().saveBinary();
    }

    @Override
    @WebMethod
    public void loadBinary(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().loadBinary();
    }

    @Override
    @WebMethod
    public void clearBinaryFile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().clearBinaryFile();
    }

    @Override
    @WebMethod
    public void saveBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().saveBase64();
    }

    @Override
    @WebMethod
    public void loadBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().loadBase64();
    }

    @Override
    @WebMethod
    public void clearBase64File(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getBackupService().clearBase64File();
    }

}
