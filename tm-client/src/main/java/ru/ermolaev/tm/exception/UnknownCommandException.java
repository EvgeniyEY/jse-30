package ru.ermolaev.tm.exception;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! This command does not exist.");
    }

    public UnknownCommandException(@NotNull final String command) {
        super("Error! This command [" + command + "] does not exist.");
    }

}
