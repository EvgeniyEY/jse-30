package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application's arguments.";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@NotNull final AbstractCommand command: commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
